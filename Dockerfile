FROM haskell:latest
ENV TINI_VERSION v0.19.0
LABEL maintainer="Sanket Naik <sanketn@gmail.com>"
RUN mkdir -p /home/project

ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
ENTRYPOINT ["/tini", "--", "tail", "-f", "/dev/null"]
WORKDIR "/home/project"